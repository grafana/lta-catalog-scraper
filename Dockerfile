FROM python:3.10

# curl is needed by pip
USER root
RUN apt-get update && apt-get install curl -y

COPY requirements.txt /
RUN pip install -r /requirements.txt

ADD lta-csv-exporter.py /lta-csv-exporter.py
WORKDIR /
ENV PYTHONPATH '/'

CMD ["python", "-u", "/lta-csv-exporter.py", "--timeout=250"]
