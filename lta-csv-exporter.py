import time
import argparse
from prometheus_client.core import GaugeMetricFamily, REGISTRY, CounterMetricFamily
from prometheus_client import start_http_server
import logging
import requests
import csv

logger = logging.getLogger()
logging.basicConfig(format="%(asctime)s %(levelname)s %(message)s", level=logging.DEBUG)

class CustomCollector(object):
    def __init__(self, user, password, timeout=250):
        self.user = user
        self.password = password
        self.timeout = timeout

        self.jar = requests.cookies.RequestsCookieJar()

    def fetch_sql_as_csv(self, query):
        logger.info(f"before: {dict(self.jar)}")
        # Login
        #self.jar.set('lta-dbview#configuration', 'project%3D2014LOFAROBS', domain='.lofar.eu', path='/')

        login_payload = {
          'domain': 'lofar.eu',
          'db_action': 'login',
          'db_name': 'db.lofar.target.rug.nl',
          'username': self.user,
          'password': self.password,
        }

        csv_request = requests.post('https://lta-login.lofar.eu/Login', auth=(self.user, self.password), cookies=self.jar, data=login_payload)

        # save session cookie
        self.jar.update(csv_request.cookies)

        # Fetch data
        params = {
          'project': '2014LOFAROBS',
          'class_str': 'HttpLofarStageRequest',
          'SQL': query,
          'Exportselect': 'CSV',
          'mode': 'table_res',
          'mainpref_fcontext_project_only': 'no',
          'set_cookie': '1',
        }

        csv_request = requests.get('https://lta-dbview.lofar.eu/DbView', params=params, auth=(self.user, self.password), cookies=self.jar)

        logger.info(f"after: {dict(self.jar)}")

        if csv_request.status_code != 200:
            raise IOError(f"Could not fetch CSV, got status code {csv_request.status_code}.")

        logger.info(f"Succesfully fetched CSV")

        return list(csv.reader(csv_request.text.split("\n")))

    def collect(self):
        """Yield all scraped metrics from all devices, as configured."""

        logger.info("Start scraping")
        scrape_begin = time.time()

        # Fetch projects
        projects = self.fetch_sql_as_csv('SELECT id, name, description FROM awoper.aweprojects')
        logger.info(f"{projects}")

        # Fetch CSV
        text = self.fetch_sql_as_csv('SELECT "+PROJECT", SUM("nr_of_files"), SUM("total_filesize") FROM AWOPER."HttpLofarStageRequest+" GROUP BY "+PROJECT"')

        # Parse CSV
        logger.debug(f"{text}")


if __name__ == "__main__":
    import sys

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-t",
        "--timeout",
        type=int,
        required=False,
        default=250,
        help="connection timeout (ms)",
    )
    parser.add_argument(
        "-p",
        "--port",
        type=int,
        required=False,
        default=8000,
        help="HTTP server port to open",
    )
    parser.add_argument(
        "-U",
        "--user",
        type=str,
        required=False,
        default="jjdmol",
        help="User name to authenticate against LTA",
    )
    parser.add_argument(
        "-P",
        "--password",
        type=str,
        required=False,
        default="0YTth76u!VHx848G@gMl",
        help="Password to authenticate against LTA",
    )
    args = parser.parse_args()

    collector = CustomCollector(user=args.user, password=args.password, timeout=args.timeout)

    logger.info("Starting server")
    start_http_server(args.port)

    logger.info("Registering collector")
    REGISTRY.register(collector)

    logger.info("Idling")
    while True:
        time.sleep(1)
